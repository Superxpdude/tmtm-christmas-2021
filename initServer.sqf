// initServer.sqf
// Executes only on the server at mission start
// No parameters are passed to this script

// Call the template initServer function
[] call XPT_fnc_initServer; // DO NOT CHANGE THIS LINE

// Call the script to handle initial task setup
[] execVM "scripts\tasks.sqf";



//////////////////////////////////////////////////////////
///// Add any mission specific code after this point /////
//////////////////////////////////////////////////////////

// Create a list of mission objects that should not be curator editable
XPT_blacklistedMissionObjects = [];

// Set the antinuke system to "captive" so it doesn't get destroyed.
{
	_x setCaptive true;
} forEach ((getMissionLayerEntities "Antinuke Launchers") select 0);
{
	_x setCaptive true;
} forEach ((getMissionLayerEntities "Antinuke Radars") select 0);

friendly_vls setCaptive true;

// Ensure that the enemy VLS is set to OPFOR
private _vlsGrp = createGroup EAST;
{[_x] joinSilent _vlsGrp} forEach (units enemy_vls);

// Set data terminal colours
{
	[_x, "red", "orange", "blue"] call BIS_fnc_dataTerminalColor;
} forEach ((getMissionLayerEntities "MEX Terminals") select 0);

setTimeMultiplier 0.1;

[] spawn {
	sleep 10;
	[parseText format ["<t align='right' size='1.6'><t font='PuristaBold' size='1.8'>%1<br/></t>%2<br/>%3</t>",
		toUpper "Supreme Caroler: Festive Alliance", 
		"by Superxpdude", 
		"16:50:00"],
		true,
		nil,
		24
	] remoteExec ["BIS_fnc_textTiles",0];
	sleep 5;
	// Play radio lines here
	[0, {
		hq_unit_highcommand sideRadio "vo_intro_01";
		hq_unit_ulysses sideRadio "vo_intro_03";	
		hq_unit_arlios sideRadio "vo_intro_04";
		hq_unit_hq sideRadio "vo_intro_02";
	}] remoteExec ["BIS_fnc_call", 0];
};