// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

//player createDiaryRecord ["Diary", ["Assets", "Example Mission Assets"]];
//player createDiaryRecord ["Diary", ["Mission", "Example Mission Briefing"]];
//player createDiaryRecord ["Diary", ["Situation", "Example Situation Briefing"]];

// TO USE THIS FILE. UNCOMMENT THE MARKED LINE IN "initPlayerLocal.sqf"

player createDiaryRecord ["Diary", ["Credits",
"A special thanks to Arlios, Schwenkdawg, Trenchgun, and Ulysses for their work on the voicelines."
]];

player createDiaryRecord ["Diary", ["Assets", "Colonel Ulysses has provided you with the following transportation:
<br/> - 6x HMMWV M2
<br/> - 7x HMMWV Transport
<br/><br/>Note that none of your vehicles will respawn if destroyed.
"]];

player createDiaryRecord ["Diary", ["Intel",
"The anti-gifters have heavy defences to the east of your objectives. Ensure that you do not proceed too far to the east, or you may encounter enemy artillery fire.
<br/><br/>Please make sure that your ""Radio"" and ""Music"" volumes are turned up. This mission does not use any copyrighted audio material, and the sounds are a big part of the experience."
]];


player createDiaryRecord ["Diary", ["Mission",
"You have landed near Gravia, to the west of one of the anti-gifters major installations. Now that you have reached Tier 3, your primary objective is to secure the Telos military base, crippling the offensive capability of the anti-gifters forces.
<br/><br/>You are being assisted by Colonel Ulysses to the south-east, and Crusader Arlios to the north-east. They will provide assistance to help you with your objective."
]];

player createDiaryRecord ["Diary", ["Situation",
"It's a week until Christmas, and things are looking dire. Anti-gifters continue to push back allied forces from their positions on Altis. Previous attempts to attack the anti-gifters have all ended in failure, leaving only one course of action.
<br/>High Command has activated the ""Supreme Caroler"" project as a last ditch effort. A joint offensive codenamed ""Festive Alliance"" has been sent to Altis to finish things for good."
]];
