// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point

// Uncomment the line below to use briefing.sqf for mission briefings. Un-needed if you're using XPTBriefings.hpp
[] execVM "scripts\briefing.sqf";

if (hasInterface) then {
	{
		[
			_x#0,
			"Secure Mass Extractor",
			"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
			"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_hack_ca.paa",
			format ["((_this distance _target) < 5) && ('%1' call BIS_fnc_taskExists) && !('%1' call BIS_fnc_taskCompleted)", _x#1],
			format ["((_caller distance _target) < 5) && ('%1' call BIS_fnc_taskExists) && !('%1' call BIS_fnc_taskCompleted)", _x#1],
			{
				params ["_target", "_caller", "_actionId", "_arguments"]; // Code Start
				if !(_arguments#0 call BIS_fnc_taskCompleted) then {
					[_target,1] remoteExec ["BIS_fnc_dataTerminalAnimate",2]
				};
			},
			{},
			{
				params ["_target", "_caller", "_actionId", "_arguments"]; // Code Completed
				[_arguments#0] remoteExec ["SXP_fnc_updateTask", 2]
			},
			{
				params ["_target", "_caller", "_actionId", "_arguments"]; // Code Interrupted
				if !(_arguments#0 call BIS_fnc_taskCompleted) then {
					[_target,0] remoteExec ["BIS_fnc_dataTerminalAnimate",2]
				};
			},
			[_x#1],
			10,
			100,
			false,
			false,
			true
		] call BIS_fnc_holdActionAdd;
	} forEach [[terminal1, "mex_1"],[terminal2, "mex_2"],[terminal3, "mex_3"]];
	
	[
		vls_terminal,
		"Launch Strategic Missile",
		"\a3\ui_f_oldman\data\IGUI\Cfg\holdactions\destroy_ca.paa",
		"\a3\ui_f_oldman\data\IGUI\Cfg\holdactions\destroy_ca.paa",
		"((_this distance _target) < 5) && ('missile_launch' call BIS_fnc_taskExists) && !('missile_launch' call BIS_fnc_taskCompleted)",
		"((_caller distance _target) < 5) && ('missile_launch' call BIS_fnc_taskExists) && !('missile_launch' call BIS_fnc_taskCompleted)",
		{},
		{},
		{
			["missile_launch"] remoteExec ["SXP_fnc_updateTask", 2]
		},
		{},
		[],
		10,
		100,
		false,
		false,
		true
	] call BIS_fnc_holdActionAdd;
};