// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{	
	class example
	{
		displayName = "Example Loadout"; // Currently unused, basically just a human-readable name for the loadout
		
		// Weapon definitions all use the following format:
		// {Weapon Classname, Suppressor, Pointer (Laser/Flashlight), Optic, [Primary magazine, ammo count], [Secondary Magazine (GL), ammo count], Bipod}
		// Any empty definitions must be defined as an empty string, or an empty array. Otherwise the loadout will not apply correctly.
		
		primaryWeapon[] = {"arifle_MXC_F", "", "acc_flashlight", "optic_ACO", {"30Rnd_65x39_caseless_mag",30}, {}, ""}; // Primary weapon definition
		secondaryWeapon[] = {"launch_B_Titan_short_F", "", "", "", {"Titan_AP", 1}, {}, ""}; // Secondary weapon (Launcher) definition.
		handgunWeapon[] = {"hgun_ACPC2_F", "", "", "", {"9Rnd_45ACP_Mag", 9}, {}, ""}; // Handgun definition
		binocular = "Binocular";
		
		uniformClass = "U_B_CombatUniform_mcam_tshirt";
		headgearClass = "H_Watchcap_blk";
		facewearClass = "";
		vestClass = "V_Chestrig_khk";
		backpackClass = "B_AssaultPack_mcamo";
		
		// Linked items requires all six definitions to be present. Use empty strings if you do not want to add that item.
		linkedItems[] = {"ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", ""}; // Linked items for the unit, must follow the order of: Map, GPS, Radio, Compass, Watch, NVGs.
		
		// When placed in an item array, magazines should also have their ammo count defined
		uniformItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in uniform. Includes weapon magazines
		vestItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in vest. Includes weapon magazines
		backpackItems[] = {{"FirstAidKit", 3}, {"30Rnd_65x39_caseless_mag", 4, 30}}; // Items to place in backpack. Includes weapon magazines
		
		basicMedUniform[] = {}; // Items to be placed in the uniform only when basic medical is being used
		basicMedVest[] = {}; // Items to be placed in the vest only when basic medical is being used
		basicMedBackpack[] = {}; // Items to be placed in the backpack only when basic medical is being used
		
		advMedUniform[] = {}; // Items to be placed in the uniform only when advanced medical is being used
		advMedVest[] = {}; // Items to be placed in the vest only when advanced medical is being used
		advMedBackpack[] = {}; // Items to be placed in the backpack only when advanced medical is being used
	};
	
	// Empty loadout with comments removed. Use this for your loadouts
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"", "", "", "", {}, {}, ""};
		binocular = "";
		
		uniformClass = "";
		headgearClass = "";
		facewearClass = "";
		vestClass = "";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", "NVGoggles"};
		
		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class B_Soldier_F: base {
		displayName = "Rifleman";

		primaryWeapon[] = {"CUP_arifle_ACRC_blk_68","","CUP_acc_Flashlight","optic_ico_01_black_f",{"CUP_30Rnd_680x43_Stanag_Tracer_Red",30},{},""};

		uniformClass = "tmtm_u_combatUniform_mcamBGrey";
		headgearClass = "tmtm_h_helmetEnhanced_grey";
		facewearClass = "CUP_G_ESS_BLK_Scarf_Face_Blk";
		vestClass = "V_PlateCarrier2_blk";

		linkedItems[] = {"ItemMap","ItemMicroDAGR","TFAR_anprc152","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"SmokeShell",2,1},{"HandGrenade",2,1},{"ItemcTabHCam",1}};
		vestItems[] = {{"CUP_30Rnd_680x43_Stanag_Tracer_Red",10,30},{"ACE_EntrenchingTool",1}};

		basicMedUniform[] = {{"ACE_fieldDressing",15},{"ACE_tourniquet",4},{"ACE_epinephrine",2},{"ACE_morphine",1}};
	};
	
	class B_Officer_F: B_Soldier_F {
		displayName = "Commander";
		
		headgearClass = "H_Beret_Colonel";
		backpackClass = "B_RadioBag_01_black_F";

		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch",""};
	};
	
	class B_medic_F: B_Soldier_F {
		displayName = "Medic";

		backpackClass = "B_TacticalPack_blk";
		
		basicMedBackpack[] = {{"ACE_fieldDressing",80},{"ACE_epinephrine",25},{"ACE_adenosine",25},{"ACE_morphine",10},{"ACE_bloodIV",10}};
	};
	
	class cmd_medic: B_medic_F {
		displayName = "Medic";

		backpackClass = "B_RadioBag_01_black_F";
		
		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch",""};
		
		basicMedBackpack[] = {{"ACE_fieldDressing",80},{"ACE_epinephrine",15},{"ACE_adenosine",10},{"ACE_morphine",5},{"ACE_bloodIV",5}};
	};
	
	class B_Soldier_TL_F: B_Soldier_F {
		displayName = "Team Leader";
		
		primaryWeapon[] = {"CUP_arifle_ACRC_EGLM_blk_68","","CUP_acc_Flashlight","optic_ico_01_black_f",{"CUP_30Rnd_680x43_Stanag_Tracer_Red",30},{"1Rnd_HE_Grenade_shell",1},""};
		
		backpackClass = "B_AssaultPack_blk";
		
		backpackItems[] = {{"1Rnd_HE_Grenade_shell",20,1}};
	};
	
	class B_Soldier_SL_F: B_Soldier_TL_F {
		displayName = "Squad Leader";
			
		backpackClass = "B_RadioBag_01_black_F";
		
		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_anprc152","ItemCompass","ItemWatch",""};
	};
	
	class B_Soldier_AR_F: B_Soldier_F {
		displayName = "Autorifleman";

		primaryWeapon[] = {"CUP_arifle_ACR_DMR_blk_556","","CUP_acc_Flashlight","optic_ico_01_black_f",{"CUP_100Rnd_TE1_Red_Tracer_556x45_BetaCMag_ar15",100},{},"bipod_01_F_blk"};

		vestItems[] = {{"ACE_EntrenchingTool",1},{"CUP_100Rnd_TE1_Red_Tracer_556x45_BetaCMag_ar15",6,100}};
	};
	
	class B_Soldier_AAR_F: B_Soldier_F {
		displayName = "Assistant Autorifleman";

		backpackClass = "B_AssaultPack_blk";
		
		backpackItems[] = {{"CUP_100Rnd_TE1_Red_Tracer_556x45_BetaCMag_ar15",8,100}};
	};
};