// Task Descriptions
// Can be used for moving task descriptions from script files to configs


// Create new classes under here. BIS_fnc_taskCreate will use either the class passed as the description parameter (in string format)
// The following would be loaded by setting the description to "example";
// If the description is blank (""), then the task ID will be used automatically
class missile_base {
	title = "Capture strategic missile base";
	description = "Anti-gifters have seized control of this strategic missile base. Recapture the base before they can get the missile system online.";
	marker = "";
};

class mex_task {
	title = "Capture Mass Extractors";
	description = "Secure the three mass extractors to provide enough mass income to get our missile defences online.";
	marker = "";
};

class mex_1 {
	title = "Secure Northern MEX";
	description = "Secure the Northern Mass Extractor.";
	marker = "";
};

class mex_2 {
	title = "Secure Central MEX";
	description = "Secure the Central Mass Extractor.";
	marker = "";
};

class mex_3 {
	title = "Secure Southern MEX";
	description = "Secure the Southern Mass Extractor.";
	marker = "";
};

class defend_mex {
	title = "Defend Mass Extractors";
	description = "Defend the mass extractors from enemy attacks until our missile is ready.";
	marker = "";
};

class missile_launch {
	title = "Launch Strategic Missile";
	description = "Launch a strategic missile to send Colonel Ulysses back to the stone age.";
	marker = "";
};