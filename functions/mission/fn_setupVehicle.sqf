// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {
	// Player vehicles
	case (toLower "CUP_B_nM1038_NATO");
	case (toLower "CUP_B_nM1025_M2_NATO"):
	{
		[_newVeh, "vehicle"] call XPT_fnc_loadItemCargo;
	};
};