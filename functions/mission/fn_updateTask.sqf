// Function for updating mission tasks when objectives are completed.
// Only execute on the server. Tasks should only be created server-side.
if (!isServer) exitWith {};

// Code for task updates goes into these fields. Can be any code required, including new task creation, task state updates, etc.
switch (toLower (_this select 0)) do {
	case "missile_base_captured": {
		if ("missile_base" call BIS_fnc_taskCompleted) exitWith {};
		["missile_base", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		
		// Move the respawn position up
		"respawn_west" setMarkerPos base_respawn_pos;
		
		// Play radio lines here
		/*
		hq_unit_highcommand sideRadio "vo_intro_01";
			hq_unit_hq sideRadio "vo_intro_02";
			hq_unit_ulysses sideRadio "vo_intro_03";
			hq_unit_arlios sideRadio "vo_intro_04";
		*/
		
		[] spawn {
			[0, {
				hq_unit_hq sideRadio "vo_bob_01";
				hq_unit_highcommand sideRadio "vo_bob_02";
			}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 6;
			
			["fake"] remoteExec ["SXP_fnc_endMissionFake", 0];
			
			sleep 13;
			
			[0, {
				hq_unit_arlios sideRadio "vo_bob_03";
			}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 5;
			
			[] remoteExec ["SXP_fnc_endMissionFakeCancel", 0];
			[0, {[player, player] call ace_medical_treatment_fnc_fullHeal;}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 5;
			
			[0, {
				hq_unit_arlios sideRadio "vo_bob_04";
				hq_unit_ulysses sideRadio "vo_bob_05";
				hq_unit_highcommand sideRadio "vo_bob_06";
				hq_unit_ulysses sideRadio "vo_bob_07";
			}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 18;
			
			[0, {
				hq_unit_highcommand sideRadio "vo_bob_08";
				hq_unit_hq sideRadio "vo_bob_09";
			}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 7;
			
			[0, {
				hq_unit_ulysses sideRadio "vo_bob_10";
			}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 6;
				
			["strategic_launch"] remoteExec ["playSound", 0];
			
			sleep 3; 
			
			[0, {
				hq_unit_hq sideRadio "vo_launch_01";
				hq_unit_arlios sideRadio "vo_launch_02";
			}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 14;
			
			[0, {
				addCamShake [1,6,25];
				playSound "distant_explosion";
			}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 4;
			
			["LeadTrack02_F_EXP"] remoteExec ["playMusic", 0];
			
			sleep 1;
			
			[0, {
				hq_unit_hq sideRadio "vo_exp_01";
			}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 5;
			
			[0, {
				hq_unit_highcommand sideRadio "vo_exp_02";
				hq_unit_hq sideRadio "vo_exp_03";
				hq_unit_hq sideRadio "vo_exp_04";
				hq_unit_hq sideRadio "vo_exp_05";
			}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 12.5;
			
			// Create additional tasks
			[
				true,
				"mex_task",
				"mex_task",
				objNull,
				"ASSIGNED",
				0,
				true,
				"use",
				true
			] call BIS_fnc_taskCreate;
			[
				true,
				["mex_1", "mex_task"],
				"mex_1",
				terminal1,
				"CREATED",
				70,
				false,
				"intel",
				true
			] call BIS_fnc_taskCreate;
			[
				true,
				["mex_2", "mex_task"],
				"mex_2",
				terminal2,
				"CREATED",
				60,
				false,
				"intel",
				true
			] call BIS_fnc_taskCreate;
			[
				true,
				["mex_3", "mex_task"],
				"mex_3",
				terminal3,
				"CREATED",
				50,
				false,
				"intel",
				true
			] call BIS_fnc_taskCreate;
			sleep 10;
			["Command Note: Please send all squads (except yourself) out to the Mass Extractors. There are no enemy units that will attack the military base after this point."] remoteExec ["systemChat", grp_cmd];
			["Command Note: Please send all squads (except yourself) out to the Mass Extractors. There are no enemy units that will attack the military base after this point."] remoteExec ["hintSilent", grp_cmd];
		};
	};
	
	case "mex_1": {
		if ("mex_1" call BIS_fnc_taskCompleted) exitWith {};
		["mex_1", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[terminal1,3] call BIS_fnc_dataTerminalAnimate;
		["mex_captured"] call SXP_fnc_updateTask;
		[terminal1, (getMissionLayerEntities "North MEX Spawns") select 0] call SXP_fnc_attackWaves;
		
	};
	
	case "mex_2": {
		if ("mex_2" call BIS_fnc_taskCompleted) exitWith {};
		["mex_2", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[terminal2,3] call BIS_fnc_dataTerminalAnimate;
		["mex_captured"] call SXP_fnc_updateTask;
		[terminal2, (getMissionLayerEntities "Centre MEX Spawns") select 0] call SXP_fnc_attackWaves;
	};
	
	case "mex_3": {
		if ("mex_3" call BIS_fnc_taskCompleted) exitWith {};
		["mex_3", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[terminal3,3] call BIS_fnc_dataTerminalAnimate;
		["mex_captured"] call SXP_fnc_updateTask;
		[terminal3, (getMissionLayerEntities "South MEX Spawns") select 0] call SXP_fnc_attackWaves;
	};
	
	case "mex_captured": {
		switch ({_x call BIS_fnc_taskCompleted} count ["mex_1", "mex_2", "mex_3"]) do {
			case 1: {
				// Play audio clip
				[hq_unit_hq, "vo_mex_01"] remoteExec ["sideRadio", 0];
			};
			case 2: {
				// Play audio clip
				[hq_unit_hq, "vo_mex_02"] remoteExec ["sideRadio", 0];
			};
			case 3: {
				["mex_task", "SUCCEEDED", true] call BIS_fnc_taskSetState;
				{
					[_x,1] remoteExec ["setVehicleAmmoDef", _x];
				} forEach ((getMissionLayerEntities "Antinuke Launchers") select 0);
				[hq_unit_hq, "vo_mex_03"] remoteExec ["sideRadio", 0];
				
				[] spawn {
					sleep 10;
					[
						true,
						"defend_mex",
						"defend_mex",
						objNull,
						"ASSIGNED",
						0,
						true,
						"defend",
						true
					] call BIS_fnc_taskCreate;
					[hq_unit_hq, "vo_defend_01"] remoteExec ["sideRadio", 0];
				};
				
				["Music_Russian_Theme"] remoteExec ["playMusic", 0];
				
				[] spawn {
					private _startTime = cba_missionTime;
					waitUntil {cba_missionTime > (_startTime + 250)};
					["enemy_missile"] call SXP_fnc_updateTask;
				};
			};
		};
	};
	
	case "enemy_missile": {
		[] spawn {
			
			[0, {
				hq_unit_ulysses sideRadio "vo_winter_01";
			}] remoteExec ["BIS_fnc_call", 0];
			sleep 6;
					
			// Launch missile
			east reportRemoteTarget [friendly_vls, 180];
			enemy_vls fireAtTarget [friendly_vls];
			// Play strategic launch sound
			["strategic_launch"] remoteExec ["playSound", 0];
			["Music_Arrival"] remoteExec ["playMusic", 0];
			
			sleep 3; 
			
			[0, {
				hq_unit_hq sideRadio "vo_winter_02";
			}] remoteExec ["BIS_fnc_call", 0];
			
			[] spawn {
				waitUntil {(!isNil "SXP_missile_intercepted_time") && {cba_missionTime > ((missionNamespace getVariable ["SXP_missile_intercepted_time",1e10]) + 30)}};
				["missile_ready"] call SXP_fnc_updateTask;
			};
		};
	};
	
	case "missile_ready": {
		[] spawn {
			["defend_mex", "SUCCEEDED", true] call BIS_fnc_taskSetState;
			[hq_unit_highcommand, "vo_missile_01"] remoteExec ["sideRadio", 0];
			sleep 5;
			[vls_terminal,2] call BIS_fnc_dataTerminalAnimate;
			[hq_unit_hq, "vo_missile_02"] remoteExec ["sideRadio", 0];
			[
				true,
				["missile_launch"],
				"missile_launch",
				vls_terminal,
				"AUTOASSIGNED",
				50,
				true,
				"use",
				true
			] call BIS_fnc_taskCreate;
			missionNamespace setVariable ["SXP_mission_ending", true, true];
		};
	};
	
	case "missile_launch": {
		[] spawn {
			["missile_launch", "SUCCEEDED", true] call BIS_fnc_taskSetState;
			
			[0, {
				0 fadeMusic 0;
				playMusic ["OM_Intro",14];
				10 fadeMusic 1;
			}] remoteExec ["BIS_fnc_call", 0];
			
			[vls_terminal,3] call BIS_fnc_dataTerminalAnimate;
			private _alarm = createSoundSource ["Sound_alarm2", position friendly_vls, [], 0];
			sleep 7.8;
			
			[vls_terminal,0] call BIS_fnc_dataTerminalAnimate;
			deleteVehicle _alarm;
			
			west reportRemoteTarget [enemy_vls, 180];
			friendly_vls fireAtTarget [enemy_vls];
			
			["strategic_launch"] remoteExec ["playSound", 0];
			
			sleep 18;
			
			[hq_unit_ulysses, "vo_end_01"] remoteExec ["sideRadio", 0];
			
			sleep 6;
			
			[0, {
				addCamShake [1,6,25];
				playSound "distant_explosion";
			}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 8;
			
			[0, {
				hq_unit_hq sideRadio "vo_end_02";
				hq_unit_highcommand sideRadio "vo_end_03";
			}] remoteExec ["BIS_fnc_call", 0];
			
			sleep 8;
			
			["victory", true, true, false] remoteExec ["BIS_fnc_endMission", 0];
		};
	};
};