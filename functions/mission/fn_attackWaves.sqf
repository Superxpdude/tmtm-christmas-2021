/*
	SXP_fnc_attackWaves
	Spawns enemies in to constantly attack a given position.
*/
if (!isServer) exitWith {};

params [
	["_target", objNull, [objNull]],
	["_spawnPositions", [], [[]]]
];

if (isNull _target) exitWith {};
if ((count _spawnPositions) <= 0) exitWith {};
if (!isNil "SXP_mission_ending") exitWith {};

private _unitList = [];

while {isNil "SXP_mission_ending"} do {
	// Start our loop
	{
		private _grp = [getPos _x, independent, (configFile >> "CfgGroups" >> "Indep" >> "CUP_I_NAPA" >> "Infantry" >> "CUP_I_NAPA_InfSquad")] call BIS_fnc_spawnGroup;
		_grp deleteGroupWhenEmpty true;
		{
			_x setSkill ["aimingAccuracy", 0];
		} forEach (units _grp);
		private _wp = _grp addWaypoint [_target, 20];
		_wp setWayPointType "MOVE";
		_grp setBehaviour "AWARE";
		{
			_x addCuratorEditableObjects [units _grp, true];
		} forEach allCurators;
		_unitList append (units _grp);
	} forEach _spawnPositions;
	waitUntil {({alive _x} count _unitList) <= ((count _spawnPositions) * 3)};
};