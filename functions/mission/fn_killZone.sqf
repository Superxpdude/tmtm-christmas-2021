/*
	SXP_fnc_killZone
	Kills player units that enter a zone after a given delay.
*/

params [
	["_unit", player, [objNull]],
	["_trigger", objNull, [objNull]],
	["_delay", 10, [0]]
];

if (isNull _unit) exitWith {};
if (isNull _trigger) exitWith {};
if !(_trigger isKindOf "EmptyDetector") exitWith {};

private _count = _delay;

while {(_unit inArea _trigger) && {_count > 0}} do {
	titleText [format ["Entering artillery area! Impact in %1 seconds!", _count], "PLAIN", 0.2];
	_count = _count - 1;
	sleep 1;
	if !(_unit inArea _trigger) exitWith {};
};

titleText ["", "PLAIN", 0.01];

if (_unit inArea _trigger) then {
	private _explosion = "HelicopterExploBig" createVehicle (getPosATL _unit);
	_unit setDamage 1;
};