/*
	SXP_fnc_endMissionFakeCancel
	Cancels the fake mission ending at a specified time.
*/

//sleep 5;
	
0 fadeMusic 1;
0 fadeSound 1;

_layerNoise = "BIS_fnc_endMission_noise" call bis_fnc_rscLayer;
_layerInterlacing = "BIS_fnc_endMission_interlacing" call bis_fnc_rscLayer;
_layerStatic = "BIS_fnc_endMission_static" call bis_fnc_rscLayer;
_layerEnd = "BIS_fnc_endMission_end" call bis_fnc_rscLayer;
{_x cuttext ["","plain"]} foreach [_layerNoise,_layerInterlacing,_layerStatic,_layerEnd];
showhud true;