enemy_vls addEventHandler ["Fired", {
	//["strategic_launch"] remoteExec ["playSound", 0];
	_this spawn {
		params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile", "_gunner"];
		
		waitUntil {getPosATL _projectile # 2 > 10};
		
		// Create the drone, and attach it to the missile
		private _plane = createVehicle ["O_UAV_06_F", [0,0,10000], [], 0, "NONE"];
		_plane disableCollisionWith _projectile;
		createVehicleCrew _plane;
		_plane setFuel 0;
		_plane setVehicleAmmoDef 0;
		_plane setVehicleTIPars [1,1,1];
		_plane allowCrewInImmobile true;
		_plane setVehicleLock "LOCKED";
		(driver _plane) disableAI "ALL";
		//_plane setObjectScale 0.001; // Setobjectscale doesn't work on moving objects
		{
			_plane setObjectTextureGlobal [_forEachIndex, ""];
		} forEach (getObjectTextures _plane);
		
		// Reveal the missile to datalink
		west reportRemoteTarget [_plane, 180];
		
		[{
			(_this select 0) params ["_projectile", "_plane"];
			if (!alive _projectile or {!alive _plane} or {(_projectile distance friendly_vls) < 250}) exitWith {
				private _explosion = "HelicopterExploBig" createVehicle (getPosATL _projectile);
				deleteVehicle _projectile;
				deleteVehicle _plane;
				[_this select 1] call CBA_fnc_removePerFrameHandler;
				
				missionNamespace setVariable ["SXP_missile_intercepted_time", cba_missionTime, true];
			};
			//[0,10,1];
			_plane setPos (_projectile modelToWorld [0,10,2]);
			_plane setVelocity (velocity _projectile);
			_plane setVectorDirAndUp [vectorDir _projectile, vectorUp _projectile];
		}, 0, [_projectile, _plane]] call CBA_fnc_addPerFrameHandler;
		
		
	};
}];